const Employee = require('../models/employee')

const {
  success
} = require('../helpers/handler')

exports.addEmployee = async (req, res, next) => {
  const {
    fullname,
    phone_number,
    salary
  } = req.body

  try {
    let employee = await Employee.create({
      fullname,
      phone_number,
      salary
    })
    success(res, 201, employee)
  } catch (error) {
    next(error)
  }
}

exports.getAllEmployee = async (req, res, next) => {
  try {
    let employee = await Employee.find()
    success(res, 200, employee)
  } catch (error) {
    next(error)
  }
}

exports.getEmployeeById = async (req, res, next) => {
  const { id } = req.params
  try {
    let employee = await Employee.findById(id)
    success(res, 200, employee)
  } catch (error) {
    next(error)
  }
}

exports.updateEmployee = async (req, res, next) => {
  const { id } = req.params
  try {
    let employee = await Employee.findByIdAndUpdate(id, { $set: req.body }, { new: true })
    success(res, 200, employee)
  } catch (error) {
    next(error)
  }
}

exports.deleteEmployee = async (req, res, next) => {
  const { id } = req.params
  try {
    let employee = await Employee.findByIdAndRemove(id)
    success(res, 200, employee)
  } catch (error) {
    next(error)
  }
}


const mongoose = require('mongoose')
const Schema = mongoose.Schema

const employeeSchema = new Schema({
  fullname: {
    type: String,
    required: true
  },
  phone_number: {
    type: String,
    required: true
  },
  salary: {
    type: Number,
    required: true
  }
})

const Employee = mongoose.model('Employee', employeeSchema)
module.exports = Employee
const express = require('express')
const router = express.Router()
const employee = require('../controllers/employee')

router.post('/add', employee.addEmployee)
router.get('/getAll', employee.getAllEmployee)
router.put('/update/:id', employee.updateEmployee)
router.delete('/delete/:id', employee.deleteEmployee)
router.get('/getById', employee.getEmployeeById)

module.exports = router
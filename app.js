require('dotenv').config()
const express = require('express')
const cors = require('cors')
const path = require('path')

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

const employeesRouter = require('./routes/employee')

app.use('/employees', employeesRouter)

app.use(function (req, res, next) {
  res.status(404).send('The page you are looking for is not found')
})

app.use(function (err, req, res, next) {
  const { statusCode = 400, message } = err

  res.status(statusCode).json({
    status: false,
    statusCode,
    message
  })
})

module.exports = app